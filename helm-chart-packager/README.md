# HELM CHART PACKAGER

This pipeline does package a helm chart, push it to a helm repo and create a tag and a release on Gitlab project

## How to use this pipeline :

Just include this file in your `.gitlab-ci.yml`, like this : 

```yaml
include:
  remote: 'https://gitlab.com/infraBuilder/templates/gitlab-ci/raw/main/helm-chart-packager/.gitlab-ci.yml'
```

## How is it working ?

This pipeline is based on a dynamically generated subpipeline for each chart that have a modification on Chart.yaml

For each chart modified, a job will check if chart in current version is not already present on helm repository, 
if not it will package the chart, and push the chart on the Helm repository.


If your "charts" directory is not at the root path of your project, you can set the variable CHART_ROOT_PATH. For example:
```yaml
variables:
  # Path of "charts" folder, 
  CHART_ROOT_PATH: "mysubpath/"
````
Please note that the variable "CHART_ROOT_PATH" must finish with a "/"

By default the helm repo is your project own helm repo served by gitlab, but you can override following variables 
in your project/group settings to customize helm repo target :
```yaml
variables:
  # Use custom helm repo instead of gtilab project repo
  HELM_REPO_URL: https://harbor.mycompany.com/
  HELM_USERNAME: myuser
- HELM_PASSWORD: mypasswd
```
Please note that default values are inherited from job execution environment, thus targeting your gitlab projet helm repo

Also, for security reasons, we strongly recommend to use robot accounts (for Harbor) or equivalent to give access to your helm repo

## Gitlab-ci features used :

- dynamic child pipeline (https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html#dynamic-child-pipelines)
- parallel:matrix jobs (https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix)
- artifacts (https://docs.gitlab.com/ee/ci/yaml/#artifactspaths)
- release (https://docs.gitlab.com/ee/ci/yaml/#release)
- rules:changes (https://docs.gitlab.com/ee/ci/yaml/#ruleschanges)

# Container image used in jobs: 

Container image : https://hub.docker.com/r/infrabuilder/kubectl
Source : infrabuilder/kubectl (https://gitlab.com/infraBuilder/kubectl)
