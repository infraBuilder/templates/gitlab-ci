# infraBuilder templates for gitlab-ci

This git repository contains Gitlab-ci templates that can be include/copied for your pipelines

## Available templates :

- [helm-chart-packager](helm-chart-packager) - This pipeline does package a helm chart, push it to a helm repo and create a tag and a release on Gitlab project
